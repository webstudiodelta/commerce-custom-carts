<?php

/**
 * @file
 * Default rule configurations for Cart.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function request_sample_default_rules_configuration() {
  $rules = array();

  // Add a reaction rule to display the default Add to Cart message.
  $rule = rules_reaction_rule();

  $rule->label = t('Display an Add to Request a Sample message');
  $rule->active = TRUE;

  $rule
    ->event('request_sample_product_add')
    ->action('request_sample_add_to_sample_message', array(
      'commerce_product:select' => 'commerce-product',
    ));

  $rules['request_sample_add_to_sample_message'] = $rule;

  // Add a reaction rule to update a request a sample order's status to "Shopping
  // cart" when a product is added to or removed from the order.
  $rule = rules_reaction_rule();

  $rule->label = t('Reset the request sample order status on product add or remove');
  $rule->active = TRUE;

  $rule
    ->event('request_sample_product_add')
    ->event('request_sample_product_remove')
    ->action('commerce_order_update_status', array(
      'commerce_order:select' => 'commerce-order',
      'order_status' => 'request_sample_cart',
    ));

  $rules['request_sample_order_status_reset'] = $rule;

  // Add a reaction rule to unset the price of disabled products in the cart
  // during price calculation, effectively removing them from the order.
  $rule = rules_reaction_rule();

  $rule->label = t('Unset the price of disabled products in request sample');
  $rule->active = TRUE;
  $rule->weight = 10;

  $rule
    ->event('commerce_product_calculate_sell_price')
    ->condition(rules_condition('data_is_empty', array(
      'data:select' => 'commerce-line-item:line-item-id',
    ))->negate())
    ->condition('entity_has_field', array(
      'entity:select' => 'commerce-line-item',
      'field' => 'commerce_product',
    ))
    ->condition('data_is', array(
      'data:select' => 'commerce-line-item:commerce-product:status',
      'op' => '==',
      'value' => '0',
    ))
    ->action('data_set', array(
      'data:select' => 'commerce-line-item:commerce-unit-price:amount',
    ));

  $rules['request_sample_unset_disabled_products'] = $rule;

  
    $rules['rules_send_a_sample_notification_e_mail'] = entity_import('rules_config', '{ "rules_send_a_sample_notification_e_mail" : {
    "LABEL" : "Send a sample request notification e-mail",
    "PLUGIN" : "reaction rule",
    "WEIGHT" : "4",
    "REQUIRES" : [ "rules", "request_sample" ],
    "ON" : [ "request_sample_checkout_complete" ],
    "DO" : [
      { "mail" : {
          "to" : [ "commerce-order:mail" ],
          "subject" : "Sample request #[commerce-order:order-number] at [site:name]",
          "message" : "Thank you we have received your submission of request #[commerce-order:order-number] at [site:name].\r\n\r\nIf this is your first order or inquiry with us, you will receive a separate e-mail with login instructions. You can view your order history or contact support at any time by logging into our website at:\r\n\r\n[site:login-url]\r\n\r\nYou can find the status of your current request at:\r\n\r\n[commerce-order:customer-url]\r\n\r\nPlease contact us if you have any questions.",
          "language" : [ "" ]
        }
      }
    ]
  }
}');
    
  
  return $rules;
}
