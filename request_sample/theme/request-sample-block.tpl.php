<?php

/**
 * @file
 * Default implementation of the request a sample block template.
 *
 * Available variables:
 * - $contents_view: A rendered View containing the contents of the cart.
 *
 * Helper variables:
 * - $order: The full order object for the request a sample.
 *
 * @see template_preprocess()
 * @see template_process()
 */
?>
<div class="cart-contents">
  <?php print $contents_view; ?>
</div>
