<?php

/**
 * Area handler to display the empty text message for request a samples.
 */
class request_sample_handler_area_empty_text extends views_handler_area {

  function render($empty = FALSE) {
    // Empty request a sample Views pages (and any variant of) will output the
    // theme for empty request a sample pages.
    if ($this->view->display_handler instanceOf views_plugin_display_page) {
      $theme_hook = 'request_sample_empty_page';
    }

    // All other display handlers (that includes blocks, attachments, content
    // panes, etc.) will fallback to using the block variant of the empty
    // request a sample theme.
    else {
      $theme_hook = 'request_sample_empty_block';
    }

    return theme($theme_hook);
  }
}
