<?php

/**
 * Provide request a sample related Views integration.
 */

/**
 * Implements hook_views_data_alter().
 */
function request_sample_views_data_alter(&$data) {
  $data['commerce_product']['request_sample_form'] = array(
    'field' => array(
      'title' => t('Request a Sample form'),
      'help' => t('Display an Request a Sample form for the product.'),
      'handler' => 'request_sample_handler_field_add_to_sample_form',
    ),
  );

  $data['commerce_order']['cart_empty_text'] = array(
    'title' => t('Empty Request a Sample'),
    'help' => t('Displays an appropriate empty text message for request a samples.'),
    'area' => array(
      'handler' => 'request_sample_handler_area_empty_text',
    ),
  );

}

/**
 * Implements hook_views_plugins().
 */
function request_sample_views_plugins() {
  return array(
    'argument default' => array(
      'request_sample_current_sample_order_id' => array(
        'title' => t("Current user's cart order ID"),
        'handler' => 'request_sample_plugin_argument_default_current_sample_order_id',
        'path' => drupal_get_path('module', 'request_sample') . '/includes/views/handlers'
      )
    )
  );
}
