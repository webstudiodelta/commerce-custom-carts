<?php

/**
 * @file
 * The page and form callbacks for use by the request a sample.
 */


/**
 * Redirects invalid checkout attempts or displays the checkout form if valid.
 */
function request_sample_checkout_router() {
  global $user;

  // Load the request a sample order.
  if ($order = request_sample_order_load($user->uid)) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
  }

  // If no request a sample order could be found, redirect away from checkout.
  // TODO: Redirect to the cart page instead which would then appear as an
  // empty request a sample page.
  if (empty($order) || commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types()) == 0) {
    drupal_set_message(t('Add some items to your cart and then try checking out.'));
    drupal_goto(variable_get('commerce_checkout_empty_redirect', ''));
  }

  drupal_goto('checkout/' . $order->order_id);
}

/**
 * Displays the request a sample form and associated information.
 */
function request_sample_view() {
  //dpm('veiw called');
  global $user;

  // Default to displaying an empty message.
  $content = theme('request_sample_empty_page');

  // First check to make sure we have a valid order.
  if ($order = request_sample_order_load($user->uid)) {
    //dpm($order);
    $wrapper = entity_metadata_wrapper('commerce_order', $order);

    // Only show the cart form if we found product line items.
    if (commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types()) > 0) {

      // Add the form for editing the cart contents.
      $content = commerce_embed_view('request_sample_form', 'default', array($order->order_id), 'request_sample');
    }
  }

  return $content;
}
