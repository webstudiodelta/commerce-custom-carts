<?php

/**
 * @file
 * Provides metadata for the request a sample order.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function request_sample_entity_property_info_alter(&$info) {
  // Add the current user's request a sample to the site information.
  $info['site']['properties']['current_sample_order'] = array(
    'label' => t("User's request a sample order"),
    'description' => t('The request a sample order belonging to the current user.'),
    'getter callback' => 'request_sample_get_properties',
    'type' => 'commerce_order',
  );
}
