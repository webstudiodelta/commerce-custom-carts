<?php

/**
 * @file
 * Provides metadata for the request a quote order.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function request_quote_entity_property_info_alter(&$info) {
  // Add the current user's request a quote to the site information.
  $info['site']['properties']['current_quote_order'] = array(
    'label' => t("User's request a quote order"),
    'description' => t('The request a quote order belonging to the current user.'),
    'getter callback' => 'request_quote_get_properties',
    'type' => 'commerce_order',
  );
}
