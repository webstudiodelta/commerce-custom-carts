<?php

/**
 * @file
 * The page and form callbacks for use by the request a quote.
 */


/**
 * Redirects invalid checkout attempts or displays the checkout form if valid.
 *//*
function request_quote_checkout_router() {
  global $user;

  // Load the request a quote order.
  if ($order = request_quote_order_load($user->uid)) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
  }

  // If no request a quote order could be found, redirect away from checkout.
  // TODO: Redirect to the cart page instead which would then appear as an
  // empty request a quote page.
  if (empty($order) || commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types()) == 0) {
    drupal_set_message(t('Add some items to your cart and then try checking out.'));
    drupal_goto(variable_get('commerce_checkout_empty_redirect', ''));
  }

  drupal_goto('request_quote_checkout/' . $order->order_id);
}*/

/**
 * Displays the request a quote form and associated information.
 */
function request_quote_view() {
  global $user;

  // Default to displaying an empty message.
  $content = theme('request_quote_empty_page');

  // First check to make sure we have a valid order.
  if ($order = request_quote_order_load($user->uid)) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);

    // Only show the cart form if we found product line items.
    if (commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types()) > 0) {

      // Add the form for editing the cart contents.
      $content = commerce_embed_view('request_quote_form', 'default', array($order->order_id), 'request_quote');
    }
  }
 //dpm($order);
  return $content;
}
