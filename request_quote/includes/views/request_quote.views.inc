<?php

/**
 * Provide request a quote related Views integration.
 */

/**
 * Implements hook_views_data_alter().
 */
function request_quote_views_data_alter(&$data) {
  $data['commerce_product']['request_quote_form'] = array(
    'field' => array(
      'title' => t('Request a Quote form'),
      'help' => t('Display an Request a Quote form for the product.'),
      'handler' => 'request_quote_handler_field_add_to_quote_form',
    ),
  );

  $data['commerce_order']['cart_empty_text'] = array(
    'title' => t('Empty Request a Quote'),
    'help' => t('Displays an appropriate empty text message for request a quotes.'),
    'area' => array(
      'handler' => 'request_quote_handler_area_empty_text',
    ),
  );

}

/**
 * Implements hook_views_plugins().
 */
function request_quote_views_plugins() {
  return array(
    'argument default' => array(
      'request_quote_current_quote_order_id' => array(
        'title' => t("Current user's cart order ID"),
        'handler' => 'request_quote_plugin_argument_default_current_quote_order_id',
        'path' => drupal_get_path('module', 'request_quote') . '/includes/views/handlers'
      )
    )
  );
}
