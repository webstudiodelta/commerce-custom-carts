<?php

/**
 * Provide request a quote related Views integration.
 */

/**
 * Implements hook_views_data_alter().
 */
function uc_alter_commerce_views_data_alter(&$data) {
  $data['commerce_product']['uc_alter_commerce_form'] = array(
    'field' => array(
      'title' => t('Combined Commerce Actions'),
      'help' => t('Display a custom combined action form for the product.'),
      'handler' => 'uc_alter_commerce_handler_field_add_to_cart_form',
    ),
  );

}

/**
 * Implements hook_views_plugins().
 */
function uc_alter_commerce_views_plugins() {
  return array(
    'argument default' => array(
      'uc_alter_commerce_current_cart_order_id' => array(
        'title' => t("Current user's cart order ID"),
        'handler' => 'uc_alter_commerce_plugin_argument_default_current_cart_order_id',
        'path' => drupal_get_path('module', 'uc_alter_commerce') . '/includes/views/handlers'
      )
    )
  );
}
