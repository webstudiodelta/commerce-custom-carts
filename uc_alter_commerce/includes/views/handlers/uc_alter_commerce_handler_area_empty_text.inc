<?php

/**
 * Area handler to display the empty text message for request a quotes.
 */
class uc_alter_commerce_handler_area_empty_text extends views_handler_area {

  function render($empty = FALSE) {
    // Empty request a quote Views pages (and any variant of) will output the
    // theme for empty request a quote pages.
    if ($this->view->display_handler instanceOf views_plugin_display_page) {
      $theme_hook = 'uc_alter_commerce_empty_page';
    }

    // All other display handlers (that includes blocks, attachments, content
    // panes, etc.) will fallback to using the block variant of the empty
    // request a quote theme.
    else {
      $theme_hook = 'uc_alter_commerce_empty_block';
    }

    return theme($theme_hook);
  }
}
